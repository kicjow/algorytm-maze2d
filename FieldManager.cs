﻿using algorytmy_itp.Graphs;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace algorytmy_itp
{
    public class FieldManager
    {
        public static Field[,] Fields;
        private static Random random;
        private static int Sidelenght;
        private static Queue<Field> Kolejka;
        private static PictureBox czyKlikniety = null;
        public static List<Coords> ResultList;
        private static Coords startCoords;

        static FieldManager()
        {
            random = new Random();
            Kolejka = new Queue<Field>();

            ResultList = new List<Coords>();

        }


        public static void GenerateFields(Panel panel)
        {
            int height = 40;
            int sidelenght = 15;
            Sidelenght = sidelenght;
            int startingPoint = 100;
            Fields = new Field[15, 15];

            for (int i = 0; i < sidelenght; i++)
            {
                for (int j = 0; j < sidelenght; j++)
                {
                    var point = new Point(height * i + startingPoint, height * j + startingPoint);
                    var picturebox = new PictureBox();
                    picturebox.Click += picturebox_Click;
                    picturebox.Location = point;
                    picturebox.Height = height;
                    picturebox.Width = height;
                    picturebox.BorderStyle = BorderStyle.FixedSingle;

                    int det = random.Next(0, 101);
                    var field = new Field();

                    if (det % 2 == 0 && det > 70) // zeby bylo wiecej zielonych
                    {
                        picturebox.BackColor = Color.Red;
                        field.Passability = FieldPassability.Impassable;
                    }
                    else
                    {
                        field.Passability = FieldPassability.Passable;
                        picturebox.BackColor = Color.Green;
                    }
                    field.FieldImage = picturebox;

                    Fields[i, j] = field;
                    field.Coords = new Coords(i, j);
                    panel.Controls.Add(picturebox);
                }
            }

        }

        static void picturebox_Click(object sender, EventArgs e)
        {
            if (czyKlikniety == null)
            {
                czyKlikniety = sender as PictureBox;
            }
            else
            {

                var szukany = FindByPictureBox(sender as PictureBox);
                var start = FindByPictureBox(czyKlikniety);
                startCoords = new Coords(start.Coords.X, start.Coords.Y);

                ColorTheFastestWay(start.Coords.X, start.Coords.Y, szukany.Coords.X, szukany.Coords.Y);
                czyKlikniety = null;
            }
        }


        public static void ColorTheFastestWay(int startX, int startY, int endX, int endY)
        {

            var startField = Fields[startX, startY];
            startField.Value = 0;
            var V = new Coords(startX, startY);


            NumberNeighbours(startX, startY, endX, endY);



            // CreateFastestWay(endCoords.X, endCoords.Y, startCoords.X, startCoords.Y);

        }

        public static void NumberNeighbours(int startX, int startY, int endX, int endY)
        {
            var startField = Fields[startX, startY];
            Kolejka.Enqueue(startField);
            bool czyZnalezionoEnd = false;

            while (Kolejka.Count > 0)
            {
                var v = Kolejka.Dequeue();
                Field topField = new Field(); // Field consists from ImageBox, its Coordinates and Value
                if (v.Coords.Y != 0)
                {
                    topField = Fields[v.Coords.X, v.Coords.Y - 1];
                    if (topField.Value == -1 && topField.Passability == FieldPassability.Passable) // All fields have value -1 at start, so -1 mean that this field is unseen.
                    {
                        topField.Value = v.Value + 1;
                        // label to see the effect
                        var label = new Label();
                        label.Text = topField.Value.ToString();
                        topField.FieldImage.Controls.Add(label);
                        Kolejka.Enqueue(topField);

                        if (v.Coords.X == endX && v.Coords.Y - 1 == endY)
                        {
                            czyZnalezionoEnd = true;
                            break;
                        }


                    }

                }
                //bot
                Field botField = new Field();
                if (v.Coords.Y != Sidelenght - 1)
                {
                    botField = Fields[v.Coords.X, v.Coords.Y + 1];
                    if (botField.Value == -1 && botField.Passability == FieldPassability.Passable)
                    {
                        botField.Value = v.Value + 1;

                        var label = new Label();
                        label.Text = botField.Value.ToString();
                        botField.FieldImage.Controls.Add(label);
                        Kolejka.Enqueue(botField);

                        if (v.Coords.X == endX && v.Coords.Y + 1 == endY)
                        {
                            czyZnalezionoEnd = true;
                            break;
                        }


                    }

                }

                //left
                Field leftField = new Field();
                if (v.Coords.X != 0)
                {
                    leftField = Fields[v.Coords.X - 1, v.Coords.Y];
                    if (leftField.Value == -1 && leftField.Passability == FieldPassability.Passable)
                    {
                        leftField.Value = v.Value + 1;

                        var label = new Label();
                        label.Text = leftField.Value.ToString();
                        leftField.FieldImage.Controls.Add(label);
                        Kolejka.Enqueue(leftField);

                        if (v.Coords.X - 1 == endX && v.Coords.Y == endY)
                        {
                            czyZnalezionoEnd = true;
                            break;
                        }


                    }
                }

                //right
                Field rightField = new Field();
                if (v.Coords.X != Sidelenght - 1)
                {
                    rightField = Fields[v.Coords.X + 1, v.Coords.Y];
                    if (rightField.Value == -1 && rightField.Passability == FieldPassability.Passable)
                    {
                        rightField.Value = v.Value + 1;
                        // test
                        var label = new Label();
                        label.Text = rightField.Value.ToString();
                        rightField.FieldImage.Controls.Add(label);

                        Kolejka.Enqueue(rightField);
                        if (v.Coords.X + 1 == endX && v.Coords.Y == endY)
                        {
                            czyZnalezionoEnd = true;
                            break;
                        }
                    }
                }
            }

            if (czyZnalezionoEnd)
            {
                var W = new Coords(endX, endY);
                
                ResultList.Add(W);
                while (W.X != startX || W.Y != startY)
                {
                    var tempfield = Fields[W.X, W.Y];
                    W = GetNeighbourWithLowestValue(tempfield.Coords.X, tempfield.Coords.Y, tempfield.Value);
                    ResultList.Add(W);
                }

                foreach (var item in ResultList)
                {
                    var field = Fields[item.X, item.Y];
                    field.FieldImage.BackColor = Color.Blue;
                }
            }
            else
            {
                MessageBox.Show("brak dojscia");
            }


        }

        //private static void NumberNeighbours2(int x, int y, int endX, int endY)
        //{


        //    bool czytop = false, czybot = false, czyright = false, czyleft = false;
        //    var thisvalue = Fields[x, y].Value;
        //    Field topField = new Field(); // Field consists from ImageBox, its Coordinates and Value
        //    if (y != 0)
        //    {
        //        topField = Fields[x, y - 1];
        //        if (topField.Value == -1 && topField.Passability == FieldPassability.Passable) // All fields have value -1 at start, so -1 mean that this field is unseen.
        //        {
        //            topField.Value = thisvalue + 1;
        //            // label to see the effect
        //            var label = new Label();
        //            label.Text = topField.Value.ToString();
        //            topField.FieldImage.Controls.Add(label);


        //            if (x == endX && y - 1 == endY)
        //            {
        //                //so its end element
        //            }
        //            czytop = true; //if czytop = false that means that this field (topfield) is outside of map or is inaccessable

        //        }

        //    }
        //    //bot
        //    Field botField = new Field();
        //    if (y != Sidelenght - 1)
        //    {
        //        botField = Fields[x, y + 1];
        //        if (botField.Value == -1 && botField.Passability == FieldPassability.Passable)
        //        {
        //            botField.Value = thisvalue + 1;

        //            var label = new Label();
        //            label.Text = botField.Value.ToString();
        //            botField.FieldImage.Controls.Add(label);


        //            if (x == endX && y + 1 == endY)
        //            {
        //                //todo
        //            }
        //            czybot = true;

        //        }

        //    }

        //    //left
        //    Field leftField = new Field();
        //    if (x != 0)
        //    {
        //        leftField = Fields[x - 1, y];
        //        if (leftField.Value == -1 && leftField.Passability == FieldPassability.Passable)
        //        {
        //            leftField.Value = thisvalue + 1;

        //            var label = new Label();
        //            label.Text = leftField.Value.ToString();
        //            leftField.FieldImage.Controls.Add(label);


        //            if (x - 1 == endX && y == endY)
        //            {

        //            }
        //            czyleft = true;

        //        }
        //    }

        //    //right
        //    Field rightField = new Field();
        //    if (x != Sidelenght - 1)
        //    {
        //        rightField = Fields[x + 1, y];
        //        if (rightField.Value == -1 && rightField.Passability == FieldPassability.Passable)
        //        {
        //            rightField.Value = thisvalue + 1;
        //            // test
        //            var label = new Label();
        //            label.Text = rightField.Value.ToString();
        //            rightField.FieldImage.Controls.Add(label);

        //            //Queue.Add(rightField);
        //            if (x + 1 == endX && y == endY)
        //            {
        //                //todo
        //            }
        //            czyright = true;

        //        }
        //    }

        //    if (czytop)
        //    {
        //        NumberNeighbours2(topField.Coords.X, topField.Coords.Y, endX, endY);
        //    }
        //    if (czybot)
        //    {
        //        NumberNeighbours2(botField.Coords.X, botField.Coords.Y, endX, endY);
        //    }
        //    if (czyleft)
        //    {
        //        NumberNeighbours2(leftField.Coords.X, leftField.Coords.Y, endX, endY);
        //    }
        //    if (czyright)
        //    {
        //        NumberNeighbours2(rightField.Coords.X, rightField.Coords.Y, endX, endY);
        //    }

        //}

        public static Coords GetNeighbourWithLowestValue(int x, int y, int value)
        {

            //top
            if (y != 0)
            {
                var topField = Fields[x, y - 1];
                if (value - topField.Value == 1)
                {

                    return new Coords(topField.Coords.X, topField.Coords.Y);
                }


            }
            //bot
            if (y != Sidelenght - 1)
            {
                var botField = Fields[x, y + 1];
                if (value - botField.Value == 1)
                {

                    return new Coords(botField.Coords.X, botField.Coords.Y);
                }

            }

            //left
            if (x != 0)
            {
                var leftField = Fields[x - 1, y];
                if (value - leftField.Value == 1)
                {

                    return new Coords(leftField.Coords.X, leftField.Coords.Y);
                }

            }

            //right
            if (x != Sidelenght - 1)
            {
                var rightField = Fields[x + 1, y];
                if (value - rightField.Value == 1)
                {

                    return new Coords(rightField.Coords.X, rightField.Coords.Y);
                }
            }

            //var items = new List<Field>();
            //foreach (var item in Fields)
            //{
            //    items.Add(item);
            //}

            //var minusjednynki = items.Where(k => k.Value == -1);
            MessageBox.Show("Brak dojscia");
            return null;

        }


        //public static void CreateFastestWay(int endX, int endY, int startX, int startY)
        //{

        //    var endField = Fields[endX, endY];

        //    LastSeenField = new Field();
        //    LastSeenField.Value = endField.Value;
        //    LastSeenField.Coords = new Coords(endField.Coords.X, endField.Coords.Y);
        //    var W = new Coords(endX, endY);
        //    ResultList.Add(W);
        //    while (W.X != startX && W.Y != startY)
        //    {
        //        W = GetNeighbourWithLowestValue(LastSeenField.Coords.X, LastSeenField.Coords.Y, LastSeenField.Value);
        //        ResultList.Add(W);
        //    }

        //    foreach (var item in ResultList)
        //    {
        //        var field = Fields[item.X, item.Y];
        //        field.FieldImage.BackColor = Color.Blue;
        //    }
        //}


        public static Field FindByPictureBox(PictureBox box)
        {
            foreach (var item in Fields)
            {
                if (item.FieldImage == box)
                {
                    return item;
                }
            }
            return null;
        }

        public static void ResetFields()
        {
            foreach (var item in Fields)
            {
                item.Value = -1;
            }

        }

    }
}
