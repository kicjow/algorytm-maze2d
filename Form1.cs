﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace algorytmy_itp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Width = 900;
            this.Height = 900;

            AddPanelAndFields();
        }

        void AddPanelAndFields()
        {
            var panel = new Panel();
            panel.BackColor = Color.Azure;
            this.Controls.Add(panel);
            panel.Dock = DockStyle.Fill;

            FieldManager.GenerateFields(panel);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
