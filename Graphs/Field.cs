﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace algorytmy_itp.Graphs
{
    public enum FieldPassability
    {
        Passable,
        Impassable
    }
            

    public class Field
    {
        public Field()
        {
            Value = -1;
            //this.Coords = new Coords(-1, -1);
        }
        public PictureBox FieldImage { get; set; }
        public FieldPassability Passability { get; set; }
        public int Value { get; set; }
        public Coords Coords { get; set; }
    }
}
